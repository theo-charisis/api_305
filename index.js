var fs = require('fs')
var restify = require('restify')
var mongo = require('./schema.js')
var server = restify.createServer()
//Aquire files from the same folder to load in here//
var dictionary = require('./dictionary.js') 


server.use(restify.fullResponse())
server.use(restify.queryParser())
server.use(restify.bodyParser())
server.use(restify.authorizationParser())



////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* This route provides a URL for each dictionary resource. It includes a parameter (indicated by a :). The string entered here is stored in the req.params object and can be used by the script. */
server.get('/', function(req, res, next) {
  res.redirect('/dictionary', next)
})



//////////////////////////////////////////////////////////////////////////////////
/* This route provides a URL for each dictionary resource. It includes a parameter (indicated by a :). The string entered here is stored in the req.params object and can be used by the script. */
server.get('/dictionary', function(req, res) {
  console.log('GET /dictionary')
  /* Here we store the id we want to retrieve in an 'immutable variable'. */
  var dictionaryID = req.query.q;
   console.log('q='+dictionaryID)
   
  /* Notice that all the business logic is kept in the 'dictionary' module. This stops the route file from becoming cluttered and allows us to implement 'unit testing' (we cover this in a later topic) */
  const data = dictionary.search(dictionaryID, function(data) {
    res.setHeader('Content-Type', 'application/json')
    res.send(data.code, data.response)
    res.end()
  })
  
})
/////////////////////////////////////////////////////////////////////
// const stdin = process.openStdin()

// stdin.on('data', function(chunk) {
//   console.log(typeof chunk)
//   var text = chunk.toString().trim()
//   console.log(typeof text)
//  if (text.indexOf('add ') === 0) {
//     var space = text.indexOf(' ')
//     var item = text.substring(space+1).trim()
//     console.log('adding "'+item+'"')
//     /* notice the use of 'arrow function' syntax to define the anonymous function parameter. */
//     mongo.addNew(item, data => {
//         console.log('returned: '+data)
//     })
//   }
// })


/////////////////////////////////////////////////////////////////////////////////////////////
/* This route point to the "dictionary" collection. The POST method indicates that me want to add a new resource to the collection.  */
server.post('/dictionary', function(req, res) {
     console.log('POST /dictionary')
      /* Since we are using the authorization parser plugin we gain an additional object which contains the information from the 'Authorization' header extracted into useful information. Here we are displaying it in the console so you can understand its structure. */
     const auth = req.authorization
     /* The req object contains all the data associated with the request received from the client. The 'body' property contains the request body as a string. */
     const body = req.body
     console.log(body)
     console.log(auth)
     const host = req.headers.host
     console.log(typeof req.files)
     const data = mongo.addNew(auth, body)
     //dictionary.add(host, auth, body, req.files, function(data) {
         console.log('DATA RETURNED')
         console.log(data)
     //const data = dictionary.addNew(body,auth)
         res.setHeader('content-type', 'application/json');
         res.send(data.code, data.response)
         res.end()
// })
})

// server.post('/dictionary', function(req, res,next) {
// //Post data into dataabse   
//   var dictionary = new mongo({
//     wor: req.body.word,
//     text: req.body.text
//   })
//   dictionary.save(function (err, dictionary) {
//     if (err) { 
//       console.log("Error Cannot Store Post data to database ")
//       return next(err) }
      
//       console.log("Successfully stored stock data in database.   "+dictionary)
//     res.json(201, {"Added": dictionary})
//   })
// })
/////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////
/* The PUT method is used to update  a named resource that already exists but is also used to create a NEW RESOURCE at the named URL.     */
server.put('/dictionary/:dictionaryID', function(req, res) {
  const dictionaryID = req.params.query;
  const data = dictionary.search(dictionaryID, function(data) {
  res.setHeader('Content-Type', 'application/json')
  res.send(data.code, {status: data.status, message: 'this should update the specified resource'})
  res.end()
})
})
//////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
/* The DELETE method removes the resource at the specified URL. */
server.del('/dictionary/:dictionaryID', function(req, res) {
  const dictionaryID = req.params.query;
  const data = dictionary.search(dictionaryID, function(data) {
  res.setHeader('Content-Type', 'application/json')
  res.send(data.code, {status: data.status, message: 'this should delete the specified resource'})
  res.end()
})
})
///////////////////////////////////////////////////////////////////


var port = process.env.PORT || 8080;
server.listen(port, function (err) {
    if (err) {
        console.error(err);
    } 
    else {
        console.log('App is ready at : ' + port);
    }
})
/////////////////////////////////////////////////////////