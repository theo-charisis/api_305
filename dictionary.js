var unirest = require('unirest');
var rand = require('csprng')
var request = require("request");
var builder = require('xmlbuilder')
var mongo = require('./schema.js')
var storage = require('node-persist')
var dictionarys = []
/* Private function that can be accessed from inside the module. It checks that the Json data supplied in the request body comprises a single array of strings. */
function validateJson(json) {
  /*  */
  if (typeof json.word !== 'string') {
    console.log('name not a string')
    return false
  }
  /* returns false if the dictionary key is not an Array */
  if (!Array.isArray(json.dictionary)) {
    console.log('json.list is not an array')
    return false
  }
  /*  */
  for(var i=0; i<json.dictionarys.length; i++) {
    if (typeof json.dictionary[i] !== 'string') {
      console.log('not a string')
      return false
    }
  }
  /* otherwise return true */
  return true
}



// These code snippets use an open-source library.
unirest.get("https://montanaflynn-dictionary.p.mashape.com/define?word=mobile")
.header("X-Mashape-Key", "8idVIqb3TDmshmo7FXD4dLajm7bVp1sKg4NjsnLnskcziR6O71")
.header("Accept", "application/json")
.end(function (result) {
  console.log(result.status, result.headers, result.body);
});

exports.search = function(query,callback) {
  console.log('search')
  if (typeof query !== 'string' || query.length === 0) {
    callback({code:400, response:{status:'error', message:'missing query (q parameter)'}})
  }
  const url = 'https://montanaflynn-dictionary.p.mashape.com/define?word=' + query;
  console.log("URL IS ", url)
  const query_string = {q: query}
  unirest.get(url)
  .header("X-Mashape-Key", "8idVIqb3TDmshmo7FXD4dLajm7bVp1sKg4NjsnLnskcziR6O71")
  .header("Accept", "application/json")
  .end(function (result) {
    //CALLBACKS HERE
    console.log(result.status, result.headers, result.body);
    callback({code:200, response:{status:'success', data:result}})
  });
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
exports.getByID = function(dictionaryID) {
  console.log('getById: '+dictionaryID)
  /* Here we loop through the dictionary . If we find one that matches the supplied id it is returned. */
  for(var i=0; i < dictionarys.length; i++) {
    if (dictionarys[i].id === dictionaryID) {
      /* Note that every public method in this module returns an object with the same properties. This consistency makes the routes file simpler. */
      return {code:200, response:{status:'success', contentType:'application/json', message:'dictionary found', data: dictionarys[i]}}
    }
  }
  /* If there are no matching dictionaries a 'resource not found' error is returned. */
  return {code:406, response:{status:'error', contentType:'application/json', message:'dictionary not found', data: dictionaryID}}
}





exports.getAll = function(host) {
  console.log('getAll')
  mongo.dictionary.find(function(err, data) {
    console.log('mongo list')
    if (err) {
      console.log('err: '+err)
        return {code:400, contentType:'application/json', response:{status:'error', message:'dictionarys not found', data: err}}
    }
    console.log('getAll data: '+data)
        return {code:200, contentType:'application/json', response:{status:'success', message:'dictionarys found', data: data}}
  })
  
}

///////////////////////////////////////////////////////////////////



exports.getAllXML = function(host) {
  console.log('getAllXML')
  /* Here the xmlbuilder module is used to construct an XML document. */
  var xml = builder.create('root', {version: '1.0', encoding: 'UTF-8', standalone: true})
  /* If there are no dictionarys in the array we build a single message node. */
  if (dictionarys.length === 0) {
    xml.ele('message', {status: 'error'}, 'no dictionarys found')
  } else {
    /* If there are dictionarys we build a different message node. */
    xml.ele('message', {status: 'success'}, 'dictionarys found')
    /* Then we create a 'dictionarys' node which will be used to contain each list node. */
    var xmlLists = xml.ele('dictionarys', {count: dictionarys.length})
    /* We now loop through the dictionarys, create a list node for each and add the details. */
    for(var i=0; i < dictionarys.length; i++) {
      var list = xmlLists.ele('list', {id: dictionarys[i].id})
      list.ele('word', dictionarys[i].word)
      list.ele('link', {href:'http://'+host+'/dictionarys/'+dictionarys[i].id})
    }
  }
  /* Once the XML document is complete we call 'end()' to turn it into an XML string. */
  xml.end({pretty: true})
  /* There is a logical error in this code, can you spot it? */
  return {code: 200, contentType:'application/xml', response: xml}
}








/////////////////////////////////////////////////////////////////////////

function addDictionary(user) {
  console.log('ADD Dictionary')
  return new Promise(function(resolve, reject) {
    const newId = rand(160, 36)
    const doc = {id:newId, user:user}
    dictionarys.push(doc)
    return resolve({code: 200, response:{status:'success', message:'document created', data:doc}})
  })
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
mongo.addNew = function(auth, body) {
  console.log('addNew')
  /* The first parameter should contain the authorization data. We check that it contains an object called 'basic' */
  if (auth.basic === undefined) {
    return {code: 401, contentType:'application/json', response:{ status:'error', message:'missing basic auth' }}
  }
  /* In this simple example we have hard-coded the username and password. You should be storing this somewhere are looking it up. */
  if (auth.basic.username !== 'testuser' || auth.basic.password !== 'p455w0rd') {
    return {code: 401, contentType:'application/json', response:{ status:'error', message:'invalid credentials' }}
  }
  /* The second parameter contains the request body as a 'string'. We need to turn this into a JavaScript object then pass it to a function to check its structure. */
  const json = JSON.parse(body)
  const valid = validateJson(json)
  /* If the 'validateJson()' function returns 'false' we need to return an error. */
  if (valid === false) {
    return {code: 400 ,contentType:'application/json', response:{ status:'error', message:'JSON data missing in request body' }}
  }
  /* In this example we use the csprng module to generate a long random string. In a production system this might be generated by the chosen data store. */
  const newId = rand(160, 36)
  /* We now create an object and push it onto the array. */
  const newDictionary = {id: newId, word:  json.word, text: json.text}
  dictionarys.push(newDictionary)
  /* And return a success code. */
  return {code: 201, contentType:'application/json', response:{ status:'success', message:'new list added', data: newDictionary }}
}








/////////////////////////////////////////////////////////////////////////





function authorisationSent(auth) {
  console.log('CHECK AUTH PRESENT')
  return new Promise(function(resolve, reject) {
    console.log('  a')
    if (auth.scheme !== 'Basic') {
      console.log('  b')
      console.log('basic auth not supplied')
      return reject({code: 401, response:{ status:'error', message:'Basic access authentication required'} })
    }
    if (auth.basic.username === undefined || auth.basic.password === undefined) {
      console.log('  c')
      console.log('missing username or password')
      return reject({code: 401, response:{ status:'error', message:'missing username or password'} })
    }
    console.log('  d')
    return resolve({code: 200, response:{ status:'success', message:'authorization header has been supplied'} })
  })
}

/////////////////////////////////////////////////////////////////////////////////////
















/////////////////////////////////////////////////////////////
   

//// POSTMAN URL: https://api-305-theo1995.c9users.io/dictionary?q=hello