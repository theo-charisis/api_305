var mongoose = require('mongoose')
//mongoose.connect('mongodb://localhost/my_database');
const server = 'mongodb://'+process.env.IP+'/api'
console.log('connect to server: '+server)
mongoose.connect(server)
const db = mongoose.connection
var Schema = mongoose.Schema
var storage = require('node-persist')
storage.initSync()

const dictionary = new Schema({
    word: { type: String, required: true },
    text:  {type: String,required:true}
});


exports.dictionary = mongoose.model('dictionary', dictionary)
////////////////////////////////////////////////////////////////////////////////


exports.addDictionary = (data, callback) => {
  console.log('dictionary')
  /* here we extract the data from the supplied string. This would be more concise if NodeJS supported the 'destructuring assignment'. */
  const step1 = data.split(':')
  console.log(step1)
  const word = step1[0]
  /* here we use the 'map' function to loop through the array and apply the trim() function. There are several useful functions that are part of the 'array prototype' such as map(), filter(), reduce(). */
  const text = step1[1].split(',').map(function(text) {
    return text.trim()
  })
  /* now we have extracted the data we can use it to create a new 'dictionary' object that adopts the correct schema. */
  const newdictionary = new dictionary({ word: word, text: text })
  newdictionary.save( (err, data) => {
    if (err) {
      callback('error: '+err)
    }
    callback('added: '+data)
  })
}
exports.getAll = callback => {
  /* the dictionary object contains several useful properties. The 'find' property contains a function to search the MongoDB document collection. */
  dictionary.find( (err, data) => {
    if (err) {
      callback('error: '+err)
    }
    const dictionary = data.map( text => {
      return {id: text._id, name: text.name}
    })
    callback(dictionary)
  })
}